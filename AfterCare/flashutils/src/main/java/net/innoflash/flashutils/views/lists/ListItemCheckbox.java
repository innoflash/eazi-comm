package net.innoflash.flashutils.views.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.pkmmte.view.CircularImageView;

import net.innoflash.flashutils.R;

public class ListItemCheckbox extends LinearLayout {

    private ImageView imageView;
    private TextView headingTextView;
    private TextView contentTextView;
    private TextView footerTextView;
    private CircularImageView circularImageView;
    private CheckBox checkBox;
    private String itemHeading;
    private String itemContent;
    private String itemFooter;
    private int icon;
    private boolean circularImage;
    private boolean checked;

    public ListItemCheckbox(Context context) {
        super(context);
        init();
    }

    public ListItemCheckbox(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ListItemCheckbox);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setIcon(typedArray.getResourceId(R.styleable.ListItemCheckbox_checkbox_list_icon, R.drawable.square));
        setItemHeading(typedArray.getString(R.styleable.ListItemCheckbox_checkbox_list_heading));
        setItemContent(typedArray.getString(R.styleable.ListItemCheckbox_checkbox_list_content));
        setItemFooter(typedArray.getString(R.styleable.ListItemCheckbox_checkbox_list_footer));
        setCircularImage(typedArray.getBoolean(R.styleable.ListItemCheckbox_checkbox_list_circular_image, true));
        setChecked(typedArray.getBoolean(R.styleable.ListItemCheckbox_checkbox_list_checked, false));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.ios_list_item_checkbox, this);
        imageView = findViewById(R.id.item_icon);
        circularImageView = findViewById(R.id.circular_image_view);
        headingTextView = findViewById(R.id.item_heading);
        contentTextView = findViewById(R.id.item_content);
        footerTextView = findViewById(R.id.item_footer);
        checkBox = findViewById(R.id.the_check_box);
    }

    public ImageView getImageView() {
        if (circularImage)
            return circularImageView;
        return imageView;
    }

    public TextView getHeadingTextView() {
        return headingTextView;
    }

    public TextView getContentTextView() {
        return contentTextView;
    }

    public TextView getFooterTextView() {
        return footerTextView;
    }

    public CircularImageView getCircularImageView() {
        return circularImageView;
    }

    public String getItemHeading() {
        return itemHeading;
    }

    public String getItemContent() {
        return itemContent;
    }

    public String getItemFooter() {
        return itemFooter;
    }

    public int getIcon() {
        return icon;
    }

    public boolean isCircularImage() {
        return circularImage;
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setItemHeading(String itemHeading) {
        this.itemHeading = itemHeading;
        headingTextView.setText(itemHeading);
    }

    public void setItemContent(String itemContent) {
        this.itemContent = itemContent;
        contentTextView.setText("| " + itemContent);
    }

    public void setItemFooter(String itemFooter) {
        this.itemFooter = itemFooter;
        footerTextView.setText(itemFooter);
    }

    public void setIcon(int icon) {
        this.icon = icon;
        imageView.setImageResource(icon);
        getCircularImageView().setImageResource(icon);
    }

    public void setCircularImage(boolean circularImage) {
        this.circularImage = circularImage;
        if (circularImage) {
            getCircularImageView().setVisibility(VISIBLE);
            imageView.setVisibility(GONE);
        } else {
            getCircularImageView().setVisibility(GONE);
            imageView.setVisibility(VISIBLE);
        }
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
        if (checked)
            checkBox.setVisibility(VISIBLE);
        else
            checkBox.setVisibility(GONE);
        checkBox.setChecked(checked);
    }
}
