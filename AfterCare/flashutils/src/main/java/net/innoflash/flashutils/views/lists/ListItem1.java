package net.innoflash.flashutils.views.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import net.innoflash.flashutils.R;

public class ListItem1 extends LinearLayout {

    private TextView listTextView;
    private String listText;

    public ListItem1(Context context) {
        super(context);
        init();
    }

    public ListItem1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ListItem1);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setListText(typedArray.getString(R.styleable.ListItem1_ios_list_one_text));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.ios_list_item_1, this);
        listTextView = findViewById(R.id.menu_text);
    }

    public String getListText() {
        return listText;
    }

    public void setListText(String listText) {
        this.listText = listText;
        listTextView.setText(listText);
    }

    public TextView getListTextView() {
        return listTextView;
    }
}
