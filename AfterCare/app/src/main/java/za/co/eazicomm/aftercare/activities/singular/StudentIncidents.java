package za.co.eazicomm.aftercare.activities.singular;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.innoflash.flashutils.views.iOSToolbar;

import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.adapters.IncidentsAdapter;

public class StudentIncidents extends AppCompatActivity implements iOSToolbar.OptionsClicked {

    private iOSToolbar toolbar;
    private RecyclerView incidentsListView;
    private IncidentsAdapter incidentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_incidents);

        toolbar = findViewById(R.id.toolbar);
        incidentsListView = findViewById(R.id.incidentsList);

        toolbar.setOnOptionsClicked(this);
        incidentsListView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        incidentsAdapter = new IncidentsAdapter(this);
        incidentsListView.setAdapter(incidentsAdapter);
    }

    @Override
    public void optionsClicked(View view) {

    }
}
