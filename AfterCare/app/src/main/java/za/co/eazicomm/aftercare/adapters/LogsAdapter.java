package za.co.eazicomm.aftercare.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.javafaker.Faker;

import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.viewholders.LogsViewHolder;
import za.co.eazicomm.aftercare.views.LogView;

public class LogsAdapter extends RecyclerView.Adapter<LogsViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private Faker faker;

    public LogsAdapter(Context context) {
        this.context = context;
        this.faker = new Faker();
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public LogsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LogsViewHolder(layoutInflater.inflate(R.layout.list_item_log, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LogsViewHolder holder, int position) {
        holder.getLogView().setTitle("Logged by : " + faker.name().fullName());
        holder.getLogView().setDate(faker.date().birthday().toString());
        if (position % 2 == 0) {
            holder.getLogView().setLogSide(LogView.LogSide.ENTRY);
        }else
            holder.getLogView().setLogSide(LogView.LogSide.EXIT);
    }

    @Override
    public int getItemCount() {
        return 15;
    }
}
