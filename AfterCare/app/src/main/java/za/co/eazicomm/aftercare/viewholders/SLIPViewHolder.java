package za.co.eazicomm.aftercare.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import za.co.eazicomm.aftercare.views.lists.StudentListItemPlain;

public class SLIPViewHolder extends RecyclerView.ViewHolder {
    private StudentListItemPlain studentListItemPlain;
    public SLIPViewHolder(@NonNull View itemView) {
        super(itemView);
        this.studentListItemPlain = (StudentListItemPlain) itemView;
    }

    public StudentListItemPlain getStudentListItemPlain() {
        return studentListItemPlain;
    }
}
