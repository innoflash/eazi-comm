package za.co.eazicomm.aftercare.extraz.internet;

import org.itishka.gsonflatten.Flatten;

public class Pagination {
    @Flatten("meta::pagination::total")
    private int total;
    @Flatten("meta::pagination::current_page")
    private int currentPage;
    @Flatten("meta::pagination::count")
    private int count;
    @Flatten("meta::pagination::per_page")
    private int perPage;
    @Flatten("meta::pagination::total_pages")
    private int totalPages;
    @Flatten("meta::pagination::links::next")
    private String nextLink;
    @Flatten("meta::pagination::links::previous")
    private String previousLink;
    private boolean finished;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

    public String getPreviousLink() {
        return previousLink;
    }

    public void setPreviousLink(String previousLink) {
        this.previousLink = previousLink;
    }

    public boolean isFinished() {
        return getCurrentPage() == getTotalPages();
    }
}
