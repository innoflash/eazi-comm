package za.co.eazicomm.aftercare.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import za.co.eazicomm.aftercare.views.LogView;

public class LogsViewHolder extends RecyclerView.ViewHolder {
    private LogView logView;
    public LogsViewHolder(@NonNull View itemView) {
        super(itemView);
        this.logView = (LogView) itemView;
    }

    public LogView getLogView() {
        return logView;
    }
}
