package za.co.eazicomm.aftercare.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import za.co.eazicomm.aftercare.R;


public class AuthInput extends LinearLayout {

    private ImageButton authIconView;
    private EditText authInputEditText;
    private String authHint;
    private String authValue;
    private AuthInputType authInputType;
    private int authIcon;

    public AuthInput(Context context) {
        super(context);
        init();
    }

    public AuthInput(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AuthInput);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setAuthIcon(typedArray.getResourceId(R.styleable.AuthInput_auth_icon, R.drawable.ic_person_outline_black_18dp));
        setAuthHint(typedArray.getString(R.styleable.AuthInput_auth_hint));
        setAuthValue(typedArray.getString(R.styleable.AuthInput_auth_value));
        if (typedArray.hasValue(R.styleable.AuthInput_auth_input_type)) {
            switch (typedArray.getInt(R.styleable.AuthInput_auth_input_type, 0)) {
                case 0:
                    setAuthInputType(AuthInputType.LONGTEXT);
                    break;
                case 1:
                    setAuthInputType(AuthInputType.LONGTEXT);
                    break;
                case 2:
                    setAuthInputType(AuthInputType.EMAIL);
                    break;
                case 3:
                    setAuthInputType(AuthInputType.NUMBER);
                    break;
                case 4:
                    setAuthInputType(AuthInputType.PHONE);
                    break;
                case 5:
                    setAuthInputType(AuthInputType.MULTILINE);
                    break;
                case 6:
                    setAuthInputType(AuthInputType.PASSWORD);
                    break;
                case 7:
                    setAuthInputType(AuthInputType.NUMBER_PASSWORD);
                    break;
                default:
                    setAuthInputType(AuthInputType.LONGTEXT);
                    break;
            }
        } else
            setAuthInputType(AuthInputType.LONGTEXT);
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.auth_input, this);
        authIconView = findViewById(R.id.auth_icon);
        authInputEditText = findViewById(R.id.auth_input);
    }

    public String getAuthHint() {
        return authHint;
    }

    public void setAuthHint(String authHint) {
        this.authHint = authHint;
        authInputEditText.setHint(authHint);
    }

    public String getAuthValue() {
        return authValue;
    }

    public void setAuthValue(String authValue) {
        this.authValue = authValue;
        authInputEditText.setText(authValue);
    }

    public int getAuthIcon() {
        return authIcon;
    }

    public void setAuthIcon(int authIcon) {
        this.authIcon = authIcon;
        try {
            authIconView.setImageResource(authIcon);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AuthInputType getAuthInputType() {
        return authInputType;
    }

    public void setAuthInputType(AuthInputType authInputType) {
        this.authInputType = authInputType;
        switch (authInputType) {
            case TEXT:
                authInputEditText.setInputType(InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
                break;
            case EMAIL:
                authInputEditText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            case PHONE:
                authInputEditText.setInputType(InputType.TYPE_CLASS_PHONE);
                break;
            case NUMBER:
                authInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case LONGTEXT:
                authInputEditText.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                break;
            case PASSWORD:
                authInputEditText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case MULTILINE:
                authInputEditText.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
                break;
            case NUMBER_PASSWORD:
                authInputEditText.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                break;
            default:
                authInputEditText.setInputType(InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
                break;
        }
    }

    public enum AuthInputType {
        TEXT,
        LONGTEXT,
        EMAIL,
        NUMBER,
        PHONE,
        MULTILINE,
        PASSWORD,
        NUMBER_PASSWORD
    }
}
