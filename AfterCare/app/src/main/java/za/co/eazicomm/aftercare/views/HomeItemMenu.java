package za.co.eazicomm.aftercare.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import za.co.eazicomm.aftercare.R;

public class HomeItemMenu extends CardView {

    private ImageView imageView;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private String title;
    private String description;
    private int icon;

    public HomeItemMenu(@NonNull Context context) {
        super(context);
        init();
    }

    public HomeItemMenu(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.HomeItemMenu);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setIcon(typedArray.getResourceId(R.styleable.HomeItemMenu_him_icon, R.drawable.ic_person_outline_black_18dp));
        setTitle(typedArray.getString(R.styleable.HomeItemMenu_him_title));
        setDescription(typedArray.getString(R.styleable.HomeItemMenu_him_description));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.home_menu_item, this);
        imageView = findViewById(R.id.item_icon);
        titleTextView = findViewById(R.id.item_title);
        descriptionTextView = findViewById(R.id.item_description);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public TextView getDescriptionTextView() {
        return descriptionTextView;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getIcon() {
        return icon;
    }

    public void setTitle(String title) {
        this.title = title;
        getTitleTextView().setText(title);
    }

    public void setDescription(String description) {
        this.description = description;
        getDescriptionTextView().setText(description);
    }

    public void setIcon(int icon) {
        this.icon = icon;
        getImageView().setImageResource(icon);
    }
}
