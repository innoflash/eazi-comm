package za.co.eazicomm.aftercare.extraz.internet;

public class API {
    public static final String CLOUDFRONT_ROOT = "https://d1n7ftbwpmsq77.cloudfront.net/";
    public static final String REVIEWS = "reviews";
    public static final String CHANNELS = "channels";
    public static final String SEARCH_CHANNELS = "search-channels";
    public static final String CHANNEL = "channel";


    public static String getRoute(String route) {
        String baseURL = "https://api.his-word.net/api/app/";
        return baseURL + route;
    }

    public static String getImage(String imageName, String dir, int dimen) {
        return CLOUDFRONT_ROOT + dir + '/' + dimen + '-' + dimen + '/' + imageName;
    }
}
