package za.co.eazicomm.aftercare.views.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import za.co.eazicomm.aftercare.R;

public class IncidentListItem extends LinearLayout {
    private TextView subjectTextView;
    private TextView dateTextView;
    private TextView descriptionTextView;
    private TextView nameTextView;
    private String subject;
    private String date;
    private String description;
    private String name;

    public IncidentListItem(Context context) {
        super(context);
        init();
    }

    public IncidentListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.IncidentListItem);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setSubject(typedArray.getString(R.styleable.IncidentListItem_incident_subject));
        setDate(typedArray.getString(R.styleable.IncidentListItem_incident_time));
        setDescription(typedArray.getString(R.styleable.IncidentListItem_incident_description));
        setName(typedArray.getString(R.styleable.IncidentListItem_incident_name));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.single_incident_list_item, this);
        subjectTextView = findViewById(R.id.incident_subject);
        dateTextView = findViewById(R.id.incident_time);
        descriptionTextView = findViewById(R.id.incident_description);
        nameTextView = findViewById(R.id.incident_name);
    }

    public TextView getSubjectTextView() {
        return subjectTextView;
    }

    public TextView getDateTextView() {
        return dateTextView;
    }

    public TextView getDescriptionTextView() {
        return descriptionTextView;
    }

    public TextView getNameTextView() {
        return nameTextView;
    }

    public String getSubject() {
        return subject;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        getNameTextView().setText(name);
    }

    public void setSubject(String subject) {
        this.subject = subject;
        getSubjectTextView().setText(subject);
    }

    public void setDate(String date) {
        this.date = date;
        getDateTextView().setText(date);
    }

    public void setDescription(String description) {
        this.description = description;
        getDescriptionTextView().setText(description);
    }
}
