package za.co.eazicomm.aftercare.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.javafaker.Faker;

import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.extraz.Constants;
import za.co.eazicomm.aftercare.viewholders.SLIViewHolder;

public class SLIAdapter extends RecyclerView.Adapter<SLIViewHolder> {
    private Context context;
    private Faker faker;
    private LayoutInflater layoutInflater;

    public SLIAdapter(Context context) {
        this.context = context;
        this.faker = new Faker();
        this.layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public SLIViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SLIViewHolder(layoutInflater.inflate(R.layout.single_student_select, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SLIViewHolder holder, int position) {
        holder.getSelectorListItem().setText(faker.name().fullName());
        holder.getSelectorListItem().setImage(Constants.PROFILE_PICS[position % 4]);
    }

    @Override
    public int getItemCount() {
        return 30;
    }
}
