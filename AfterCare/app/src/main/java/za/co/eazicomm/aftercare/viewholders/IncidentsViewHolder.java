package za.co.eazicomm.aftercare.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import za.co.eazicomm.aftercare.views.lists.IncidentListItem;

public class IncidentsViewHolder extends RecyclerView.ViewHolder {
    private IncidentListItem incidentListItem;
    public IncidentsViewHolder(@NonNull View itemView) {
        super(itemView);
        this.incidentListItem = (IncidentListItem) itemView;
    }

    public IncidentListItem getIncidentListItem() {
        return incidentListItem;
    }
}
