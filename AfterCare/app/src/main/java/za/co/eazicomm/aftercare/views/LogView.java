package za.co.eazicomm.aftercare.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import za.co.eazicomm.aftercare.R;

public class LogView extends LinearLayout {

    public enum LogSide{
        ENTRY,
        EXIT
    }

    private ImageView imageView;
    private TextView titleTextView;
    private TextView timeTextView;
    private LogSide logSide;
    private String title;
    private String date;

    public LogView(Context context) {
        super(context);
        init();
    }

    public LogView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LogView);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setTitle(typedArray.getString(R.styleable.LogView_log_title));
        setDate(typedArray.getString(R.styleable.LogView_log_date));
        int side = typedArray.getInt(R.styleable.LogView_log_side, 0);
        if (side == 0) {
            setLogSide(LogSide.ENTRY);
        }else
            setLogSide(LogSide.EXIT);
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.log_view, this);
        imageView = findViewById(R.id.log_icon);
        titleTextView = findViewById(R.id.log_title);
        timeTextView = findViewById(R.id.log_time);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public TextView getTimeTextView() {
        return timeTextView;
    }

    public LogSide getLogSide() {
        return logSide;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public void setLogSide(LogSide logSide) {
        this.logSide = logSide;
        if (logSide == LogSide.ENTRY) {
            getImageView().setBackground(getResources().getDrawable(R.drawable.incident_round_background));
            getImageView().setImageResource(R.drawable.ic_lock_open_white_24dp);
        }else{
            getImageView().setBackground(getResources().getDrawable(R.drawable.badge_updated_background));
            getImageView().setImageResource(R.drawable.ic_lock_white_24dp);
        }
    }

    public void setTitle(String title) {
        this.title = title;
        getTitleTextView().setText(title);
    }

    public void setDate(String date) {
        this.date = date;
        getTimeTextView().setText(date);
    }
}
