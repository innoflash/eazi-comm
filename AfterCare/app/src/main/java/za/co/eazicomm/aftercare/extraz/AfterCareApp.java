package za.co.eazicomm.aftercare.extraz;

import android.app.Application;
import android.content.Context;

import nouri.in.goodprefslib.GoodPrefs;

public class AfterCareApp extends Application {

    private static AfterCareApp afterCareApp;

    @Override
    public void onCreate() {
        super.onCreate();
        GoodPrefs.init(this);
        afterCareApp = this;
    }

    public static AfterCareApp getInstance() {
        return afterCareApp;
    }

    public static Context getAppContext() {
        return afterCareApp.getApplicationContext();
    }
}
