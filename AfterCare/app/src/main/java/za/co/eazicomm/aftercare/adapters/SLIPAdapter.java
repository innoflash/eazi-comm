package za.co.eazicomm.aftercare.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.javafaker.Faker;

import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.extraz.Constants;
import za.co.eazicomm.aftercare.viewholders.SLIPViewHolder;

public class SLIPAdapter extends RecyclerView.Adapter<SLIPViewHolder> {

    private Context context;
    private Faker faker;
    private LayoutInflater layoutInflater;

    public SLIPAdapter(Context context) {
        this.context = context;
        this.faker = new Faker();
        this.layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public SLIPViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SLIPViewHolder(layoutInflater.inflate(R.layout.list_item_students_student, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SLIPViewHolder holder, int position) {
        holder.getStudentListItemPlain().setText(faker.name().fullName());
        holder.getStudentListItemPlain().setImage(Constants.PROFILE_PICS[position % 4]);
    }

    @Override
    public int getItemCount() {
        return 30;
    }
}
