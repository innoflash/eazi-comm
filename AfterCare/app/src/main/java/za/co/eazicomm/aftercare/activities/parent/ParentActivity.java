package za.co.eazicomm.aftercare.activities.parent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.flashutils.views.iOSToolbar;
import net.innoflash.flashutils.views.lists.ListItem3;

import za.co.eazicomm.aftercare.R;

public class ParentActivity extends AppCompatActivity implements View.OnClickListener {

    private ListItem3 student;
    private iOSToolbar toolbar;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);

        student = findViewById(R.id.student);
        toolbar = findViewById(R.id.toolbar);

        student.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        intent = new Intent(this, StudentActivity.class);
        startActivity(intent);
    }
}
