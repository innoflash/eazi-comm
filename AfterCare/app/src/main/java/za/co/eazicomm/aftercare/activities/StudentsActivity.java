package za.co.eazicomm.aftercare.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import net.innoflash.flashutils.recyclerview.RecyclerTouchListener;
import net.innoflash.flashutils.recyclerview.RecyclerViewClickListener;
import net.innoflash.flashutils.views.iOSToolbar;

import br.com.liveo.searchliveo.SearchLiveo;
import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.adapters.SLIPAdapter;
import za.co.eazicomm.aftercare.extraz.alerts.Alerts;

public class StudentsActivity extends AppCompatActivity implements iOSToolbar.OptionsClicked, SearchLiveo.OnSearchListener, SearchLiveo.OnHideSearchListener, RecyclerViewClickListener {

    private iOSToolbar toolbar;
    private SearchLiveo searchLiveo;
    private RecyclerView studentsListView;
    private SLIPAdapter adapter;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);
        adapter = new SLIPAdapter(this);

        toolbar = findViewById(R.id.toolbar);
        searchLiveo = findViewById(R.id.search_liveo);
        studentsListView = findViewById(R.id.studentsList);

        toolbar.setOnOptionsClicked(this);
        searchLiveo.with(this, this)
                .hideSearch(this)
                .build();
        studentsListView.setLayoutManager(new LinearLayoutManager(this));
        studentsListView.addOnItemTouchListener(new RecyclerTouchListener(this, studentsListView, this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        studentsListView.setAdapter(adapter);
    }

    @Override
    public void optionsClicked(View view) {
        toolbar.setVisibility(View.GONE);
        searchLiveo.setVisibility(View.VISIBLE);
        searchLiveo.show();
    }

    @Override
    public void changedSearch(CharSequence charSequence) {
        Alerts.longToast(this, (String) charSequence);
    }

    @Override
    public void hideSearch() {
        toolbar.setVisibility(View.VISIBLE);
        searchLiveo.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == SearchLiveo.REQUEST_CODE_SPEECH_INPUT) {
                searchLiveo.with(this).resultVoice(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onClick(View view, int position) {
        intent = new Intent(this, StudentActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLongClick(View view, int position) {

    }
}
