package za.co.eazicomm.aftercare.views.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.pkmmte.view.CircularImageView;

import za.co.eazicomm.aftercare.R;

public class SelectorListItem extends StudentListItemPlain implements View.OnClickListener {
    private CircularImageView checkIconView;
    private boolean selected;
    private Drawable icon;

    public SelectorListItem(Context context) {
        super(context);
    }

    public SelectorListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SelectorListItem);
        setSelected(typedArray.getBoolean(R.styleable.SelectorListItem_slip_selected, false));
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
        if (selected) {
            checkIconView.setVisibility(VISIBLE);
            // getImageView().setImageResource(R.drawable.check);
            setBackgroundColor(getResources().getColor(R.color.black_transparent));
        } else {
            // getImageView().setImageDrawable(this.icon);
            checkIconView.setVisibility(GONE);
            setBackgroundColor(0);
        }
    }

    @Override
    protected void init() {
        super.init();
        checkIconView = findViewById(R.id.check_icon);
        setOnClickListener(this);
        this.icon = getImageView().getDrawable();
    }

    @Override
    public void onClick(View v) {
        setSelected(!isSelected());
    }
}
