package za.co.eazicomm.aftercare.activities.singular;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.williamww.silkysignature.views.SignaturePad;

import net.innoflash.flashutils.views.iOSToolbar;

import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.extraz.alerts.Alerts;

public class CheckOutActivity extends AppCompatActivity implements View.OnClickListener, SignaturePad.OnSignedListener {

    private iOSToolbar toolbar;
    private ImageButton clear;
    private ImageButton approve;
    private SignaturePad signaturePad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        toolbar = findViewById(R.id.toolbar);
        clear = findViewById(R.id.clear_signature);
        approve = findViewById(R.id.approve_signature);
        signaturePad = findViewById(R.id.signature_pad);

        clear.setOnClickListener(this);
        approve.setOnClickListener(this);

        clear.setVisibility(View.INVISIBLE);
        approve.setVisibility(View.INVISIBLE);

        signaturePad.setOnSignedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear_signature:
                signaturePad.clear();
                break;
            case R.id.approve_signature:
                Alerts.longToast(this, "will send signature to server");
                break;
        }
    }

    @Override
    public void onStartSigning() {

    }

    @Override
    public void onSigned() {
        clear.setVisibility(View.VISIBLE);
        approve.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClear() {
        clear.setVisibility(View.INVISIBLE);
        approve.setVisibility(View.INVISIBLE);
    }
}
