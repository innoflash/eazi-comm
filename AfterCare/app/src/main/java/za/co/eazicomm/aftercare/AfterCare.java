package za.co.eazicomm.aftercare;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.flashutils.views.iOSToolbar;

import za.co.eazicomm.aftercare.activities.BulkCheckInActivity;
import za.co.eazicomm.aftercare.activities.CheckOutActivity;
import za.co.eazicomm.aftercare.activities.CheckedOutActivity;
import za.co.eazicomm.aftercare.activities.IncidentsActivity;
import za.co.eazicomm.aftercare.activities.StudentsActivity;
import za.co.eazicomm.aftercare.activities.authentication.LoginActivity;
import za.co.eazicomm.aftercare.activities.parent.ParentActivity;
import za.co.eazicomm.aftercare.extraz.Constants;
import za.co.eazicomm.aftercare.extraz.alerts.Alerts;
import za.co.eazicomm.aftercare.views.HomeItemMenu;

public class AfterCare extends AppCompatActivity implements iOSToolbar.OptionsClicked, View.OnClickListener {

    private iOSToolbar toolbar;
    private HomeItemMenu studentsHIM;
    private HomeItemMenu checkInHIM;
    private HomeItemMenu incidentsHIM;
    private HomeItemMenu checkOutHIM;
    private HomeItemMenu checkedOutHIM;
    private HomeItemMenu logOutHIM;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        studentsHIM = findViewById(R.id.my_students);
        checkInHIM = findViewById(R.id.bulk_check_in);
        incidentsHIM = findViewById(R.id.incidents);
        checkOutHIM = findViewById(R.id.check_out);
        logOutHIM = findViewById(R.id.logout);
        checkedOutHIM = findViewById(R.id.checked_out);

        intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        toolbar.setOnOptionsClicked(this);
        studentsHIM.setOnClickListener(this);
        checkInHIM.setOnClickListener(this);
        incidentsHIM.setOnClickListener(this);
        checkOutHIM.setOnClickListener(this);
        logOutHIM.setOnClickListener(this);
        checkedOutHIM.setOnClickListener(this);
    }

    @Override
    public void optionsClicked(View view) {
        intent = new Intent(this, ParentActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.my_students:
                intent = new Intent(this, StudentsActivity.class);
                startActivity(intent);
                break;
            case R.id.bulk_check_in:
                intent = new Intent(this, BulkCheckInActivity.class);
                startActivity(intent);
                break;
            case R.id.incidents:
                intent = new Intent(this, IncidentsActivity.class);
                startActivity(intent);
                break;
            case R.id.check_out:
                intent = new Intent(this, CheckOutActivity.class);
                startActivity(intent);
                break;
            case R.id.logout:
                //optionsClicked(null);
                finish();
                break;
            case R.id.checked_out:
                intent = new Intent(this, CheckedOutActivity.class);
                startActivity(intent);
                break;
        }
        Alerts.longToast(this, Constants.COMING_SOON);
    }
}
