package za.co.eazicomm.aftercare.extraz;


import za.co.eazicomm.aftercare.R;

public class Constants {
    public static final int[] PROFILE_PICS = {
            R.drawable.profile_pic,
            R.drawable.girlie,
            R.drawable.girly,
            R.drawable.boyie
    };
    public static final String CARD_TITLE = "Project description";
    public static final String LOREM_IPSUM = "From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.";
    public static final String COMING_SOON = "coming soon ....";
    public static final String CHECK_IN = "Check in";
    public static final String CHECK_OUT = "Check out";
    public static final String VIEW_INCIDENTS = "View incidents";
    public static final String ADD_INCIDENT = "Add Incident";
    public static final String STUDENT_PROFILE = "Student Profile";
    public static final String EDIT = "Edit";
}
