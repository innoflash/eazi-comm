package za.co.eazicomm.aftercare.views.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;

import za.co.eazicomm.aftercare.R;

public class StudentListItemPlain extends RelativeLayout {
    private CircularImageView imageView;
    private TextView textView;
    private String text;
    private int image;

    public StudentListItemPlain(Context context) {
        super(context);
        init();
    }

    public StudentListItemPlain(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StudentListItemPlain);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setImage(typedArray.getResourceId(R.styleable.StudentListItemPlain_slip_icon, R.drawable.getty));
        setText(typedArray.getString(R.styleable.StudentListItemPlain_slip_text));
    }

    protected void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.single_student_students_list, this);
        imageView = findViewById(R.id.item_icon);
        textView = findViewById(R.id.item_text);
    }

    public CircularImageView getImageView() {
        return imageView;
    }

    public TextView getTextView() {
        return textView;
    }

    public String getText() {
        return text;
    }

    public int getImage() {
        return image;
    }

    public void setText(String text) {
        this.text = text;
        getTextView().setText(text);
    }

    public void setImage(int image) {
        this.image = image;
        getImageView().setImageResource(image);
    }
}
