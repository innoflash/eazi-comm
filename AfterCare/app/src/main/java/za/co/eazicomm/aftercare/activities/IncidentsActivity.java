package za.co.eazicomm.aftercare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.innoflash.flashutils.views.iOSToolbar;

import br.com.liveo.searchliveo.SearchLiveo;
import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.adapters.IncidentsAdapter;
import za.co.eazicomm.aftercare.extraz.alerts.Alerts;

public class IncidentsActivity extends AppCompatActivity implements iOSToolbar.OptionsClicked, View.OnClickListener, SearchLiveo.OnSearchListener, SearchLiveo.OnHideSearchListener {

    private iOSToolbar toolbar;
    private SearchLiveo searchLiveo;
    private RecyclerView incidentsListView;
    private IncidentsAdapter adapter;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incidents);

        toolbar = findViewById(R.id.toolbar);
        searchLiveo = findViewById(R.id.search_liveo);
        incidentsListView = findViewById(R.id.incidentsList);
        fab = findViewById(R.id.fab);

        toolbar.setOnOptionsClicked(this);
        fab.setOnClickListener(this);
        incidentsListView.setLayoutManager(new LinearLayoutManager(this));
        searchLiveo.with(this, this)
                .hideSearch(this)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter = new IncidentsAdapter(this);
        incidentsListView.setAdapter(adapter);
    }

    @Override
    public void optionsClicked(View view) {
        toolbar.setVisibility(View.GONE);
        searchLiveo.setVisibility(View.VISIBLE);
        searchLiveo.show();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void changedSearch(CharSequence charSequence) {
        Alerts.longToast(this, (String) charSequence);
    }

    @Override
    public void hideSearch() {
        toolbar.setVisibility(View.VISIBLE);
        searchLiveo.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == SearchLiveo.REQUEST_CODE_SPEECH_INPUT) {
                searchLiveo.with(this).resultVoice(requestCode, resultCode, data);
            }
        }
    }
}
