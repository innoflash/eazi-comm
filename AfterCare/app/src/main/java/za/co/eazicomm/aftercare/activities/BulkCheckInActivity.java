package za.co.eazicomm.aftercare.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.innoflash.flashutils.recyclerview.RecyclerTouchListener;
import net.innoflash.flashutils.recyclerview.RecyclerViewClickListener;
import net.innoflash.flashutils.views.iOSToolbar;

import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.adapters.SLIAdapter;
import za.co.eazicomm.aftercare.extraz.alerts.Alerts;
import za.co.eazicomm.aftercare.views.lists.SelectorListItem;

public class BulkCheckInActivity extends AppCompatActivity implements iOSToolbar.OptionsClicked, RecyclerViewClickListener {

    private iOSToolbar toolbar;
    private RecyclerView studentsListView;
    private SLIAdapter adapter;
    private SelectorListItem selectorListItem;
    private TextView selected_count;
    private int selectedCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_check_in);


        toolbar = findViewById(R.id.toolbar);
        studentsListView = findViewById(R.id.studentsList);
        selected_count = findViewById(R.id.selected_count);

        toolbar.setOnOptionsClicked(this);
        studentsListView.setLayoutManager(new LinearLayoutManager(this));
        studentsListView.addOnItemTouchListener(new RecyclerTouchListener(this, studentsListView, this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter = new SLIAdapter(this);
        studentsListView.setAdapter(adapter);
    }

    @Override
    public void optionsClicked(View view) {
        Alerts.longToast(this, "Will check in all selected students!");
    }

    @Override
    public void onClick(View view, int position) {
        selectorListItem = (SelectorListItem) view;
        if (selectorListItem.isSelected())
            selectedCount--;
        else
            selectedCount++;
        selected_count.setText("Selected (" + selectedCount + ")");
    }

    @Override
    public void onLongClick(View view, int position) {

    }
}
