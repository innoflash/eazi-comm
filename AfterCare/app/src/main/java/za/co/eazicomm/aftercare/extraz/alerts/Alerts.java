package za.co.eazicomm.aftercare.extraz.alerts;

import android.content.Context;
import android.widget.Toast;

public class Alerts {
    public static void longToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
