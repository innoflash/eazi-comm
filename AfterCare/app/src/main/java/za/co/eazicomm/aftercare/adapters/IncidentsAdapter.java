package za.co.eazicomm.aftercare.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.javafaker.Faker;

import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.viewholders.IncidentsViewHolder;

public class IncidentsAdapter extends RecyclerView.Adapter<IncidentsViewHolder> {

    private Context context;
    private Faker faker;
    private LayoutInflater layoutInflater;

    public IncidentsAdapter(Context context) {
        this.context = context;
        this.faker = new Faker();
        this.layoutInflater = LayoutInflater.from(this.context);
    }

    @NonNull
    @Override
    public IncidentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new IncidentsViewHolder(layoutInflater.inflate(R.layout.list_item_incident, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull IncidentsViewHolder holder, int position) {
        holder.getIncidentListItem().setName(faker.name().fullName());
        holder.getIncidentListItem().setSubject(faker.book().title());
        holder.getIncidentListItem().setDate(faker.date().birthday().toString());
        holder.getIncidentListItem().setDescription(faker.shakespeare().romeoAndJulietQuote());
    }

    @Override
    public int getItemCount() {
        return 15;
    }
}
