package za.co.eazicomm.aftercare.activities.parent;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.MenuParams;

import net.innoflash.flashutils.views.iOSToolbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import client.yalantis.com.foldingtabbar.FoldingTabBar;
import za.co.eazicomm.aftercare.R;
import za.co.eazicomm.aftercare.activities.singular.CheckOutActivity;
import za.co.eazicomm.aftercare.activities.singular.StudentIncidents;
import za.co.eazicomm.aftercare.adapters.LogsAdapter;
import za.co.eazicomm.aftercare.extraz.Constants;
import za.co.eazicomm.aftercare.extraz.alerts.Alerts;

public class StudentActivity extends AppCompatActivity implements iOSToolbar.OptionsClicked, FoldingTabBar.OnFoldingItemSelectedListener {

    private iOSToolbar toolbar;
    private FoldingTabBar foldingTabBar;
    private RecyclerView logsListView;
    private LogsAdapter logsAdapter;
    private ContextMenuDialogFragment contextMenuDialogFragment;
    private List<MenuObject> menuObjects;
    private MenuParams menuParams;
    private MenuObject closeMenu;
    private MenuObject checkOut;
    private MenuObject viewIncidents;
    private MenuObject editProfile;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parent_student);

        toolbar = findViewById(R.id.toolbar);
        foldingTabBar = findViewById(R.id.folding_tab_bar);
        logsListView = findViewById(R.id.logsList);

        toolbar.setOnOptionsClicked(this);
        foldingTabBar.setOnFoldingItemClickListener(this);
        initMenu();
        logsListView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initMenu() {
        closeMenu = new MenuObject();
        checkOut = new MenuObject(Constants.CHECK_OUT);
        viewIncidents = new MenuObject(Constants.VIEW_INCIDENTS);
        editProfile = new MenuObject(Constants.EDIT);

        closeMenu.setResourceValue(R.drawable.ic_close_blue_18dp);
        checkOut.setResourceValue(R.drawable.ic_lock_blue_18dp);
        viewIncidents.setResourceValue(R.drawable.ic_report_problem_blue_18dp);
        editProfile.setResourceValue(R.drawable.ic_edit_blue_18dp);

        menuObjects = new ArrayList<>();
        menuObjects.add(closeMenu);
        menuObjects.add(checkOut);
        menuObjects.add(viewIncidents);
        menuObjects.add(editProfile);

        menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.toolbar_height));
        menuParams.setMenuObjects(menuObjects);
        menuParams.setClosableOutside(false);

        contextMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        contextMenuDialogFragment.setMenuItemClickListener((view, integer) -> {
            switch (integer) {
                case 0:
                    contextMenuDialogFragment.dismiss();
                    break;
                case 1:
                    checkStudentOut();
                    break;
                case 2:
                    openStudentIncidents();
                    break;
                case 3:
                    editStudentProfile();
                    break;
            }
            return null;
        });
    }

    private void editStudentProfile() {
        Alerts.longToast(this, "Will edit student details");
    }

    private void openStudentIncidents() {
        intent = new Intent(this, StudentIncidents.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        logsAdapter = new LogsAdapter(this);
        logsListView.setAdapter(logsAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (contextMenuDialogFragment != null && contextMenuDialogFragment.isAdded()) {
            contextMenuDialogFragment.dismiss();
        }
    }

    private void checkStudentOut() {
        Alerts.showToast(this, "Will check out student");
    }

    @Override
    public void optionsClicked(View view) {
        openMenu();
    }

    private void openMenu() {
        if (getSupportFragmentManager().findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
            contextMenuDialogFragment.show(getSupportFragmentManager(), ContextMenuDialogFragment.TAG);
        }
    }

    @Override
    public boolean onFoldingItemSelected(@NotNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.incidents:
                openStudentIncidents();
                break;
            case R.id.check_out:
                checkStudentOut();
                break;
        }
        return false;
    }
}
