package za.co.eazicomm.aftercare.viewholders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import za.co.eazicomm.aftercare.views.lists.SelectorListItem;

public class SLIViewHolder extends RecyclerView.ViewHolder {
    private SelectorListItem selectorListItem;

    public SLIViewHolder(@NonNull View itemView) {
        super(itemView);
        this.selectorListItem = (SelectorListItem) itemView;
    }

    public SelectorListItem getSelectorListItem() {
        return selectorListItem;
    }
}
