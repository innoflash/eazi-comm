package net.innoflash.flashutils.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.innoflash.flashutils.R;

public class iOSToolbar extends RelativeLayout implements View.OnClickListener {

    private Button backButton;
    private TextView toolbarTextView;
    private ImageButton optionsImageButton;
    private Activity activity;
    private OptionsClicked optionsClicked;
    private String pageTitle;
    private int icon;
    private boolean hasOptions;
    private boolean noBackButton;

    public iOSToolbar(Context context) {
        super(context);
        init();
    }

    public iOSToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.iOSToolbar);
        readAttributes(typedArray);
        typedArray.recycle();
    }


    private void readAttributes(TypedArray typedArray) {
        setPageTitle(typedArray.getString(R.styleable.iOSToolbar_ios_title));
        setIcon(typedArray.getResourceId(R.styleable.iOSToolbar_ios_option_icon, R.drawable.ic_more_vert_black_18dp));
        if (typedArray.hasValue(R.styleable.iOSToolbar_ios_has_options)) {
            setHasOptions(typedArray.getBoolean(R.styleable.iOSToolbar_ios_has_options, false));
        } else
            setHasOptions(false);
        setNoBackButton(typedArray.getBoolean(R.styleable.iOSToolbar_ios_no_back_button, false));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.ios_toolbar, this);
        backButton = findViewById(R.id.nav_back);
        toolbarTextView = findViewById(R.id.page_title);
        optionsImageButton = findViewById(R.id.more_actions);

        backButton.setOnClickListener(this);
        optionsImageButton.setOnClickListener(this);
    }

    public boolean isNoBackButton() {
        return noBackButton;
    }

    public void setNoBackButton(boolean noBackButton) {
        this.noBackButton = noBackButton;
        if (noBackButton) {
            backButton.setVisibility(GONE);
        }else
            backButton.setVisibility(VISIBLE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.nav_back) {
            activity = (Activity) getContext();
            activity.finish();
        } else {
            if (optionsClicked != null)
                optionsClicked.optionsClicked(this);
        }
    }

    public void setOnOptionsClicked(OptionsClicked optionsClicked) {
        this.optionsClicked = optionsClicked;
    }

    public interface OptionsClicked {
        public void optionsClicked(View view);
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
        toolbarTextView.setText(pageTitle);
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
        optionsImageButton.setImageResource(icon);
    }

    public boolean isHasOptions() {
        return hasOptions;
    }

    public void setHasOptions(boolean hasOptions) {
        this.hasOptions = hasOptions;
        if (hasOptions) {
            optionsImageButton.setVisibility(VISIBLE);
        } else {
            optionsImageButton.setVisibility(GONE);
        }
    }
}
