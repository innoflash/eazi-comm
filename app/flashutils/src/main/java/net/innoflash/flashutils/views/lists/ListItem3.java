package net.innoflash.flashutils.views.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.pkmmte.view.CircularImageView;

import net.innoflash.flashutils.R;

public class ListItem3 extends LinearLayout {

    public enum ItemType {
        READ,
        LATEST,
        UPDATED
    }

    private ImageView imageView;
    private TextView headingTextView;
    private TextView contentTextView;
    private TextView footerTextView;
    private TextView readTypeTextView;
    private CircularImageView circularImageView;
    private Button newTypeButton;
    private Button updatedTypeButton;
    private String itemHeading;
    private String itemContent;
    private String itemFooter;
    private ItemType itemType;
    private Bitmap iconBitmap;
    private int icon;
    private boolean hasType;
    private boolean circularImage;

    public ListItem3(Context context) {
        super(context);
        init();
    }

    public ListItem3(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ListItem3);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.ios_list_item_3, this);
        imageView = findViewById(R.id.item_icon);
        circularImageView = findViewById(R.id.circular_image_view);
        headingTextView = findViewById(R.id.item_heading);
        contentTextView = findViewById(R.id.item_content);
        footerTextView = findViewById(R.id.item_footer);
        readTypeTextView = findViewById(R.id.item_read);
        newTypeButton = findViewById(R.id.item_new_badge);
        updatedTypeButton = findViewById(R.id.item_updated_badge);
    }

    private void readAttributes(TypedArray typedArray) {
        setIcon(typedArray.getResourceId(R.styleable.ListItem3_item_icon, R.drawable.square));
        setItemHeading(typedArray.getString(R.styleable.ListItem3_item_heading));
        setItemContent(typedArray.getString(R.styleable.ListItem3_item_content));
        setItemFooter(typedArray.getString(R.styleable.ListItem3_item_footer));
        setHasType(typedArray.getBoolean(R.styleable.ListItem3_item_has_type, false));
        if (typedArray.hasValue(R.styleable.ListItem3_item_has_type)) {
            if (typedArray.getInt(R.styleable.ListItem3_item_has_type, 0) == 0)
                setItemType(ItemType.READ);
            else if (typedArray.getInt(R.styleable.ListItem3_item_has_type, 0) == 1)
                setItemType(ItemType.LATEST);
            else if (typedArray.getInt(R.styleable.ListItem3_item_has_type, 0) == 2)
                setItemType(ItemType.UPDATED);
        } else
            setItemType(ItemType.READ);
        setHasType(typedArray.getBoolean(R.styleable.ListItem3_item_has_type, false));
        setCircularImage(typedArray.getBoolean(R.styleable.ListItem3_item_circular_image, true));
    }

    public ImageView getImageView() {
        if (circularImage)
            return circularImageView;
        return imageView;
    }

    public TextView getHeadingTextView() {
        return headingTextView;
    }

    public TextView getContentTextView() {
        return contentTextView;
    }

    public TextView getFooterTextView() {
        return footerTextView;
    }

    public TextView getReadTypeTextView() {
        return readTypeTextView;
    }

    public Button getNewTypeButton() {
        return newTypeButton;
    }

    public Button getUpdatedTypeButton() {
        return updatedTypeButton;
    }

    public String getItemHeading() {
        return itemHeading;
    }

    public String getItemContent() {
        return itemContent;
    }

    public String getItemFooter() {
        return itemFooter;
    }

    public void setItemHeading(String itemHeading) {
        this.itemHeading = itemHeading;
        getHeadingTextView().setText(itemHeading);
    }

    public void setItemContent(String itemContent) {
        this.itemContent = itemContent;
        getContentTextView().setText("| " + itemContent);
    }

    public void setItemFooter(String itemFooter) {
        this.itemFooter = itemFooter;
        getFooterTextView().setText(itemFooter);
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
        getImageView().setImageResource(icon);
        getCircularImageView().setImageResource(icon);
    }

    public Bitmap getIconBitmap() {
        return iconBitmap;
    }

    public void setIconBitmap(Bitmap iconBitmap) {
        this.iconBitmap = iconBitmap;
        getImageView().setImageBitmap(iconBitmap);
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
        switch (itemType) {
            case READ:
                newTypeButton.setVisibility(GONE);
                updatedTypeButton.setVisibility(GONE);
                readTypeTextView.setVisibility(VISIBLE);
                break;
            case LATEST:
                newTypeButton.setVisibility(VISIBLE);
                updatedTypeButton.setVisibility(GONE);
                readTypeTextView.setVisibility(GONE);
            case UPDATED:
                newTypeButton.setVisibility(GONE);
                updatedTypeButton.setVisibility(VISIBLE);
                readTypeTextView.setVisibility(GONE);
                break;
        }
    }

    public void setHasType(boolean hasType) {
        this.hasType = hasType;
        if (!hasType) {
            readTypeTextView.setVisibility(GONE);
            newTypeButton.setVisibility(GONE);
            updatedTypeButton.setVisibility(GONE);
        }
    }

    public CircularImageView getCircularImageView() {
        return circularImageView;
    }

    public boolean isCircularImage() {
        return circularImage;
    }

    public void setCircularImage(boolean circularImage) {
        this.circularImage = circularImage;
        if (circularImage) {
            getCircularImageView().setVisibility(VISIBLE);
            imageView.setVisibility(GONE);
        } else {
            getCircularImageView().setVisibility(GONE);
            imageView.setVisibility(VISIBLE);
        }
    }
}
