package net.innoflash.eazicomm.activities.teacher.activities;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.extraz.Constants;
import net.innoflash.eazicomm.extraz.alerts.Alerts;
import net.innoflash.eazicomm.views.HomeListItem;
import net.innoflash.flashutils.views.iOSToolbar;

public class Selector extends AppCompatActivity implements View.OnClickListener {

    private iOSToolbar toolbar;
    private HomeListItem photoItem;
    private HomeListItem videoItem;
    private HomeListItem observationItem;
    private HomeListItem foodItem;
    private HomeListItem napItem;
    private HomeListItem pottyItem;
    private HomeListItem noteItem;
    private HomeListItem medsItem;
    private HomeListItem extramuralItem;
    private HomeListItem incidentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selector);

        toolbar = findViewById(R.id.toolbar);
        photoItem = findViewById(R.id.photo);
        videoItem = findViewById(R.id.video);
        observationItem = findViewById(R.id.observation);
        foodItem = findViewById(R.id.food);
        napItem = findViewById(R.id.nap);
        pottyItem = findViewById(R.id.potty);
        noteItem = findViewById(R.id.note);
        medsItem = findViewById(R.id.meds);
        extramuralItem = findViewById(R.id.extramural);
        incidentItem = findViewById(R.id.incident);

        photoItem.setOnClickListener(this);
        videoItem.setOnClickListener(this);
        observationItem.setOnClickListener(this);
        foodItem.setOnClickListener(this);
        noteItem.setOnClickListener(this);
        napItem.setOnClickListener(this);
        pottyItem.setOnClickListener(this);
        medsItem.setOnClickListener(this);
        extramuralItem.setOnClickListener(this);
        incidentItem.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photo:
                //todo open photo adding intent
                break;
            case R.id.video:
                //todo open video adding intent
                break;
            case R.id.observation:
                //todo open observation adding intent
                break;
            case R.id.food:
                //todo open food adding intent
                break;
            case R.id.nap:
                //todo open nap adding intent
                break;
            case R.id.potty:
                //todo open potty adding intent
                break;
            case R.id.note:
                //todo open note adding intent
                break;
            case R.id.meds:
                //todo open meds adding intent
                break;
            case R.id.extramural:
                //todo open extramural adding intent
                break;
            case R.id.incident:
                //todo open incident adding intent
                break;
        }
        Alerts.longToast(this, Constants.COMING_SOON);
    }
}
