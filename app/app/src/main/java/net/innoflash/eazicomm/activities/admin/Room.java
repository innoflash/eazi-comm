package net.innoflash.eazicomm.activities.admin;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.adapters.admin.StudentsAdapter;
import net.innoflash.eazicomm.extraz.alerts.Alerts;
import net.innoflash.flashutils.views.iOSToolbar;

public class Room extends AppCompatActivity implements iOSToolbar.OptionsClicked {

    private iOSToolbar toolbar;
    private RecyclerView studentsListView;
    private StudentsAdapter studentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        studentsAdapter = new StudentsAdapter(this);
        toolbar = findViewById(R.id.toolbar);
        studentsListView = findViewById(R.id.studentsList);

        studentsListView.setLayoutManager(new LinearLayoutManager(this));
        studentsListView.setAdapter(studentsAdapter);

        toolbar.setOnOptionsClicked(this);
    }

    @Override
    public void optionsClicked(View view) {
        Alerts.longToast(this, "Will add child");
    }
}
