package net.innoflash.eazicomm.activities.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.activities.authentication.register.CreateAccount;
import net.innoflash.eazicomm.activities.authentication.register.StaffOrTeacher;

public class Register extends AppCompatActivity implements View.OnClickListener {

    private Button staffOrTeacher;
    private Button familyOrApprovedPickup;
    private TextView signIn;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        staffOrTeacher = findViewById(R.id.staff_or_teacher);
        familyOrApprovedPickup = findViewById(R.id.family_or_approved_pickup);
        signIn = findViewById(R.id.already_got_account);

        staffOrTeacher.setOnClickListener(this);
        familyOrApprovedPickup.setOnClickListener(this);
        signIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.staff_or_teacher:
                intent = new Intent(this, StaffOrTeacher.class);
                startActivity(intent);
                break;
            case R.id.family_or_approved_pickup:
                intent = new Intent(this, CreateAccount.class);
                startActivity(intent);
                break;
            case R.id.already_got_account:
                intent = new Intent(this, Login.class);
                startActivity(intent);
                break;
        }
    }
}
