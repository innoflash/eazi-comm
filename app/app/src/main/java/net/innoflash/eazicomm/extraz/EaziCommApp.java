package net.innoflash.eazicomm.extraz;

import android.app.Application;
import android.content.Context;

import nouri.in.goodprefslib.GoodPrefs;

public class EaziCommApp extends Application {

    private static EaziCommApp appInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        GoodPrefs.init(getApplicationContext());
        appInstance = this;
    }

    public static EaziCommApp getInstance() {
        return appInstance;
    }

    public static Context getAppContext() {
        return appInstance.getApplicationContext();
    }
}
