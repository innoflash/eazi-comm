package net.innoflash.eazicomm.activities.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.activities.CategoryChooser;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private TextView forgotPassword;
    private Button login;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        forgotPassword = findViewById(R.id.forgot_password);
        login = findViewById(R.id.loginButton);

        forgotPassword.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgot_password:
                //to do forgot password staff
                break;
            case R.id.loginButton:
                intent = new Intent(this, CategoryChooser.class);
                startActivity(intent);
                break;
        }
    }
}
