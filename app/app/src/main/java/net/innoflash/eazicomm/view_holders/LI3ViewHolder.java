package net.innoflash.eazicomm.view_holders;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.innoflash.flashutils.views.lists.ListItem3;

public class LI3ViewHolder extends RecyclerView.ViewHolder {
    public ListItem3 listItem;
    public LI3ViewHolder(@NonNull View itemView) {
        super(itemView);
        this.listItem = (ListItem3) itemView;
    }
}
