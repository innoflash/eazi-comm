package net.innoflash.eazicomm.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import net.innoflash.eazicomm.R;

public class EaziCommActivity extends LinearLayout {

    private ImageView iconImageView;
    private ImageView activityImageView;
    private TextView descriptionTextView;
    private TextView briefTextView;
    private TextView timeTextView;
    private String description;
    private String brief;
    private String time;
    private boolean hasImage;
    private int icon;
    private int image;

    public EaziCommActivity(Context context) {
        super(context);
        init();
    }

    public EaziCommActivity(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.EaziCommActivity);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setIcon(typedArray.getResourceId(R.styleable.EaziCommActivity_activity_icon, R.drawable.ic_chat_bubble_black_24dp));
        setDescription(typedArray.getString(R.styleable.EaziCommActivity_activity_description));
        setBrief(typedArray.getString(R.styleable.EaziCommActivity_activity_brief));
        setTime(typedArray.getString(R.styleable.EaziCommActivity_activity_time));
        setImage(typedArray.getResourceId(R.styleable.EaziCommActivity_activity_image, R.drawable.girly));
        setHasImage(typedArray.getBoolean(R.styleable.EaziCommActivity_activity_has_image, false));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.single_activity_item, this);
        iconImageView = findViewById(R.id.activity_icon);
        activityImageView = findViewById(R.id.activity_image);
        descriptionTextView = findViewById(R.id.activity_description);
        briefTextView = findViewById(R.id.activity_brief);
        timeTextView = findViewById(R.id.activity_time);
    }

    public ImageView getIconImageView() {
        return iconImageView;
    }

    public ImageView getActivityImageView() {
        return activityImageView;
    }

    public TextView getDescriptionTextView() {
        return descriptionTextView;
    }

    public TextView getBriefTextView() {
        return briefTextView;
    }

    public TextView getTimeTextView() {
        return timeTextView;
    }

    public String getDescription() {
        return description;
    }

    public String getBrief() {
        return brief;
    }

    public String getTime() {
        return time;
    }

    public int getIcon() {
        return icon;
    }

    public int getImage() {
        return image;
    }

    public void setDescription(String description) {
        this.description = description;
        getDescriptionTextView().setText(description);
    }

    public void setBrief(String brief) {
        this.brief = brief;
        getBriefTextView().setText(brief);
        if (brief == null || brief.length() == 0)
            getBriefTextView().setVisibility(GONE);
    }

    public void setTime(String time) {
        this.time = time;
        getTimeTextView().setText(time);
    }

    public void setIcon(int icon) {
        this.icon = icon;
        getIconImageView().setImageResource(icon);
    }

    public void setImage(int image) {
        this.image = image;
        getActivityImageView().setImageResource(image);
    }

    public boolean isHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
        if(hasImage)
            getActivityImageView().setVisibility(VISIBLE);
        else
            getActivityImageView().setVisibility(GONE);
    }
}
