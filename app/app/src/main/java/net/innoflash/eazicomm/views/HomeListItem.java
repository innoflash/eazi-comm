package net.innoflash.eazicomm.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.innoflash.eazicomm.R;

public class HomeListItem extends RelativeLayout {

    private ImageView imageView;
    private TextView textView;
    private String listText;
    private int listIcon;


    public HomeListItem(Context context) {
        super(context);
        init();
    }

    public HomeListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.HomeListItem);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setListText(typedArray.getString(R.styleable.HomeListItem_ec_list_text));
        setListIcon(typedArray.getResourceId(R.styleable.HomeListItem_ec_list_icon, R.drawable.ic_exit_to_app_black_18dp));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.admin_home_items, this);
        imageView = findViewById(R.id.item_icon);
        textView = findViewById(R.id.item_text);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTextView() {
        return textView;
    }

    public String getListText() {
        return listText;
    }

    public void setListText(String listText) {
        this.listText = listText;
        textView.setText(listText);
    }

    public int getListIcon() {
        return listIcon;
    }

    public void setListIcon(int listIcon) {
        this.listIcon = listIcon;
        imageView.setImageResource(listIcon);
    }
}
