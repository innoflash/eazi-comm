package net.innoflash.eazicomm.activities.teacher;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.activities.parent.Student;
import net.innoflash.eazicomm.activities.teacher.activities.Selector;
import net.innoflash.eazicomm.adapters.staff.StudentsAdapter;
import net.innoflash.eazicomm.extraz.Constants;
import net.innoflash.eazicomm.extraz.alerts.Alerts;
import net.innoflash.flashutils.recyclerview.RecyclerTouchListener;
import net.innoflash.flashutils.recyclerview.RecyclerViewClickListener;
import net.innoflash.flashutils.views.iOSToolbar;

import org.jetbrains.annotations.NotNull;

import client.yalantis.com.foldingtabbar.FoldingTabBar;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, iOSToolbar.OptionsClicked, FoldingTabBar.OnFoldingItemSelectedListener, RecyclerViewClickListener {

    private iOSToolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private RecyclerView studentsListView;
    private StudentsAdapter studentsAdapter;
    private FoldingTabBar foldingTabBar;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_dashboard);

        studentsAdapter = new StudentsAdapter(this);

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        studentsListView = findViewById(R.id.studentsList);
        foldingTabBar = findViewById(R.id.folding_tab_bar);

        navigationView.setNavigationItemSelectedListener(this);
        toolbar.setOnOptionsClicked(this);
        foldingTabBar.setOnFoldingItemClickListener(this);
        studentsListView.setLayoutManager(new LinearLayoutManager(this));
        studentsListView.setAdapter(studentsAdapter);
        studentsListView.addOnItemTouchListener(new RecyclerTouchListener(this, studentsListView, this));
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switchActions(id);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void optionsClicked(View view) {
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onFoldingItemSelected(@NotNull MenuItem menuItem) {
        switchActions(menuItem.getItemId());
        return false;
    }

    private void switchActions(int itemId) {
        switch (itemId) {
            case R.id.add_activity:
                intent = new Intent(this, Selector.class);
                startActivity(intent);
                break;
            default:
                Alerts.longToast(this, Constants.COMING_SOON);
                break;
        }
    }

    @Override
    public void onClick(View view, int position) {
        intent = new Intent(this, Student.class);
        startActivity(intent);
    }

    @Override
    public void onLongClick(View view, int position) {

    }
}
