package net.innoflash.eazicomm.adapters.parent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.javafaker.Faker;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.extraz.Constants;
import net.innoflash.eazicomm.view_holders.LI3ViewHolder;

public class StudentsAdapter extends RecyclerView.Adapter<LI3ViewHolder> {

    private Context context;
    private Faker faker;

    public StudentsAdapter(Context context) {
        this.context = context;
        this.faker = new Faker();
    }

    @NonNull
    @Override
    public LI3ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LI3ViewHolder(LayoutInflater.from(context).inflate(R.layout.single_room_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LI3ViewHolder holder, int position) {
        holder.listItem.setIcon(Constants.PROFILE_PICS[position]);
        holder.listItem.setItemHeading(faker.color().name() + " room");
        holder.listItem.setItemContent(faker.name().fullName());
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
