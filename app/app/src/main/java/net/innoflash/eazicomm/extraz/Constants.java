package net.innoflash.eazicomm.extraz;

import net.innoflash.eazicomm.R;

public class Constants {
    public static final int[] PROFILE_PICS = {
            R.drawable.profile_pic,
            R.drawable.square,
            R.drawable.girly
    };
    public static final String CARD_TITLE = "Project description";
    public static final String LOREM_IPSUM = "From its medieval origins to the digital era, learn everything there is to know about the ubiquitous lorem ipsum passage.";
    public static final String COMING_SOON = "coming soon ....";
}
