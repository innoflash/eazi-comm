package net.innoflash.eazicomm.activities.authentication.invite;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;

public class Steps extends AppCompatActivity implements View.OnClickListener {

    private Button emailProvider;
    private Button emailParents;
    private Button haveUsInvite;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps);

        emailProvider = findViewById(R.id.email_provider);
        emailParents = findViewById(R.id.email_parents);
        haveUsInvite = findViewById(R.id.have_us_email);

        emailProvider.setOnClickListener(this);
        emailParents.setOnClickListener(this);
        haveUsInvite.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.email_provider:
                //todo will email provider
                break;
            case R.id.email_parents:
                //todo will email parents
                break;
            case R.id.have_us_email:
                intent = new Intent(this, EaziCommInvite.class);
                startActivity(intent);
                break;
        }
    }
}
