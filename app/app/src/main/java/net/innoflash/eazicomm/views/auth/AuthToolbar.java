package net.innoflash.eazicomm.views.auth;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import net.innoflash.eazicomm.R;


public class AuthToolbar extends LinearLayout implements View.OnClickListener {
    private ImageButton backImageButton;
    private Activity activity;

    public AuthToolbar(Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.auth_toolbar, this);
        backImageButton = findViewById(R.id.nav_back);
    }

    public AuthToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        initButtonClicks();
    }

    private void initButtonClicks() {
        backImageButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        activity = (Activity) getContext();
        activity.finish();
    }
}
