package net.innoflash.eazicomm.activities.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.extraz.alerts.Alerts;
import net.innoflash.eazicomm.views.HomeListItem;
import net.innoflash.flashutils.views.iOSToolbar;

import org.jetbrains.annotations.NotNull;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, iOSToolbar.OptionsClicked, View.OnClickListener {

    private iOSToolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private HomeListItem rooms;
    private HomeListItem messageParents;
    private HomeListItem schoolProfile;
    private HomeListItem newsletters;
    private HomeListItem logout;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        toolbar = findViewById(R.id.toolbar);
        rooms = findViewById(R.id.rooms);
        messageParents = findViewById(R.id.message_parents);
        schoolProfile = findViewById(R.id.school_profile);
        newsletters = findViewById(R.id.newsletters);
        logout = findViewById(R.id.logout);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setOnOptionsClicked(this);
        rooms.setOnClickListener(this);
        messageParents.setOnClickListener(this);
        schoolProfile.setOnClickListener(this);
        newsletters.setOnClickListener(this);
        logout.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NotNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switchAction(id);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void optionsClicked(View view) {
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(View v) {
        switchAction(v.getId());
    }

    private void switchAction(int itemID) {
        switch (itemID) {
            case R.id.rooms:
                intent = new Intent(this, Rooms.class);
                startActivity(intent);
                break;
            case R.id.logout:
                finish();
                break;
        }
    }
}
