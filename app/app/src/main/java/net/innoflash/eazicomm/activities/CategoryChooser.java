package net.innoflash.eazicomm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.activities.super_admin.Dashboard;


public class CategoryChooser extends AppCompatActivity implements View.OnClickListener {

    private Button superAdmin;
    private Button admin;
    private Button staff;
    private Button parent;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_chooser);

        superAdmin = findViewById(R.id.superAdminCategory);
        admin = findViewById(R.id.adminCategory);
        staff = findViewById(R.id.staffCategory);
        parent = findViewById(R.id.parentsCategory);

        superAdmin.setOnClickListener(this);
        admin.setOnClickListener(this);
        staff.setOnClickListener(this);
        parent.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.superAdminCategory:
                intent = new Intent(this, Dashboard.class);
                startActivity(intent);
                break;
            case R.id.adminCategory:
                intent = new Intent(this, net.innoflash.eazicomm.activities.admin.Dashboard.class);
                startActivity(intent);
                break;
            case R.id.parentsCategory:
                intent = new Intent(this, net.innoflash.eazicomm.activities.parent.Dashboard.class);
                startActivity(intent);
                break;
            case R.id.staffCategory:
                intent = new Intent(this, net.innoflash.eazicomm.activities.teacher.Dashboard.class);
                startActivity(intent);
                break;
        }
    }
}
