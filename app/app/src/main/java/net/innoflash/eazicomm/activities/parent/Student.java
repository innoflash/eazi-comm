package net.innoflash.eazicomm.activities.parent;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.extraz.alerts.Alerts;
import net.innoflash.flashutils.views.iOSToolbar;

public class Student extends AppCompatActivity implements iOSToolbar.OptionsClicked {

    private iOSToolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        toolbar = findViewById(R.id.toolbar);

        toolbar.setOnOptionsClicked(this);
    }

    @Override
    public void optionsClicked(View view) {
        Alerts.longToast(this, "Will edit profile");
    }
}
