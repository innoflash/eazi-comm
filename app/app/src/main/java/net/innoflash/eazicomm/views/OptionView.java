package net.innoflash.eazicomm.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import net.innoflash.eazicomm.R;

public class OptionView extends LinearLayout {

    private ImageView imageView;
    private TextView textView;
    private String optionTitle;
    private int optionIcon;

    public OptionView(Context context) {
        super(context);
        init();
    }

    public OptionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.OptionView);
        readAttributes(typedArray);
        typedArray.recycle();
    }

    private void readAttributes(TypedArray typedArray) {
        setOptionTitle(typedArray.getString(R.styleable.OptionView_option_title));
        setOptionIcon(typedArray.getResourceId(R.styleable.OptionView_option_icon, R.drawable.ic_assignment_ind_black_24dp));
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.single_option_item, this);
        imageView = findViewById(R.id.option_icon);
        textView = findViewById(R.id.option_title);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getTextView() {
        return textView;
    }

    public String getOptionTitle() {
        return optionTitle;
    }

    public int getOptionIcon() {
        return optionIcon;
    }

    public void setOptionTitle(String optionTitle) {
        this.optionTitle = optionTitle;
        textView.setText(optionTitle);
    }

    public void setOptionIcon(int optionIcon) {
        this.optionIcon = optionIcon;
        imageView.setImageResource(optionIcon);
    }
}
