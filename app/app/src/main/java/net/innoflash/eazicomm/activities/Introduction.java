package net.innoflash.eazicomm.activities;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.core.content.ContextCompat;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.extraz.Constants;
import net.innoflash.eazicomm.extraz.alerts.Alerts;

import java.util.ArrayList;
import java.util.List;

public class Introduction extends AhoyOnboarderActivity {

    private List<AhoyOnboarderCard> cards;
    private AhoyOnboarderCard card1;
    private AhoyOnboarderCard card2;
    private AhoyOnboarderCard card3;
    private AhoyOnboarderCard card4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cards = new ArrayList<>();
        card1 = new AhoyOnboarderCard(Constants.CARD_TITLE, Constants.LOREM_IPSUM, R.drawable.chat);
        card2 = new AhoyOnboarderCard(Constants.CARD_TITLE, Constants.LOREM_IPSUM, R.drawable.chat);
        card3 = new AhoyOnboarderCard(Constants.CARD_TITLE, Constants.LOREM_IPSUM, R.drawable.chat);
        card4 = new AhoyOnboarderCard(Constants.CARD_TITLE, Constants.LOREM_IPSUM, R.drawable.chat);

        card1.setBackgroundColor(R.color.black_transparent);
        card2.setBackgroundColor(R.color.black_transparent);
        card3.setBackgroundColor(R.color.black_transparent);
        card4.setBackgroundColor(R.color.black_transparent);

        cards.add(card1);
        cards.add(card2);
        cards.add(card3);
        cards.add(card4);

        for (AhoyOnboarderCard card : cards) {
            card.setTitleColor(R.color.white);
            card.setDescriptionColor(R.color.grey_200);
        }

        setFinishButtonTitle("Finish");
        showNavigationControls(true);
        setGradientBackground();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setFinishButtonDrawableStyle(ContextCompat.getDrawable(this, R.drawable.rounded_button));
        }

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        setFont(face);

        setOnboardPages(cards);
    }

    @Override
    public void onFinishButtonPressed() {
        Alerts.longToast(this, Constants.CARD_TITLE);
        finish();
    }
}