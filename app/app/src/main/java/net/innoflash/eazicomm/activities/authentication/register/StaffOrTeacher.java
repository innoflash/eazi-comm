package net.innoflash.eazicomm.activities.authentication.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;

public class StaffOrTeacher extends AppCompatActivity implements View.OnClickListener {

    private Button schoolOrClassroom;
    private Button joinSchool;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_or_teacher);

        schoolOrClassroom = findViewById(R.id.new_school_or_classroom);
        joinSchool = findViewById(R.id.join_your_school);

        schoolOrClassroom.setOnClickListener(this);
        joinSchool.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_school_or_classroom:
                intent = new Intent(this, CreateAccount.class);
                startActivity(intent);
                break;
            case R.id.join_your_school:
                intent = new Intent(this, CreateAccount.class);
                startActivity(intent);
                break;
        }
    }
}
