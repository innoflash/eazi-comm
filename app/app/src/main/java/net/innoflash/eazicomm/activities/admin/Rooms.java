package net.innoflash.eazicomm.activities.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.adapters.LI3Adapter;
import net.innoflash.eazicomm.extraz.alerts.Alerts;
import net.innoflash.flashutils.recyclerview.RecyclerTouchListener;
import net.innoflash.flashutils.recyclerview.RecyclerViewClickListener;
import net.innoflash.flashutils.views.iOSToolbar;

public class Rooms extends AppCompatActivity implements iOSToolbar.OptionsClicked, RecyclerViewClickListener {

    private iOSToolbar toolbar;
    private RecyclerView roomsListView;
    private LI3Adapter li3Adapter;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        li3Adapter = new LI3Adapter(this);

        toolbar = findViewById(R.id.toolbar);
        roomsListView = findViewById(R.id.roomsList);

        toolbar.setOnOptionsClicked(this);
        roomsListView.setLayoutManager(new LinearLayoutManager(this));
        roomsListView.setAdapter(li3Adapter);
        roomsListView.addOnItemTouchListener(new RecyclerTouchListener(this, roomsListView, this));
    }

    @Override
    public void optionsClicked(View view) {
        Alerts.longToast(this, "will add room");
    }

    @Override
    public void onClick(View view, int position) {
        intent = new Intent(this, Room.class);
        startActivity(intent);
    }

    @Override
    public void onLongClick(View view, int position) {

    }
}
