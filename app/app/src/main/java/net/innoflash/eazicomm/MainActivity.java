package net.innoflash.eazicomm;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import net.innoflash.eazicomm.activities.Introduction;
import net.innoflash.eazicomm.activities.authentication.InviteSchool;
import net.innoflash.eazicomm.activities.authentication.Login;
import net.innoflash.eazicomm.activities.authentication.Register;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Intent intent;
    private Button signIn;
    private Button signUp;
    private TextView inviteSchool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = new Intent(this, Introduction.class);
        startActivity(intent);

        signIn = findViewById(R.id.sign_in);
        signUp = findViewById(R.id.sign_up);
        inviteSchool = findViewById(R.id.invite_school);

        signIn.setOnClickListener(this);
        signUp.setOnClickListener(this);
        inviteSchool.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in:
                intent = new Intent(this, Login.class);
                startActivity(intent);
                break;
            case R.id.sign_up:
                intent = new Intent(this, Register.class);
                startActivity(intent);
                break;
            case R.id.invite_school:
                intent = new Intent(this, InviteSchool.class);
                startActivity(intent);
                break;
        }
    }
}
