package net.innoflash.eazicomm.activities.authentication.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.activities.CategoryChooser;


public class CreateAccount extends AppCompatActivity implements View.OnClickListener {

    private Button createAccount;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        createAccount = findViewById(R.id.create_account);
        createAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_account:
                intent = new Intent(this, CategoryChooser.class);
                startActivity(intent);
                break;
        }
    }
}
