package net.innoflash.eazicomm.activities.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.activities.authentication.invite.Steps;

public class InviteSchool extends AppCompatActivity implements View.OnClickListener {

    private Button next;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_school);
        next = findViewById(R.id.next);

        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                intent = new Intent(this, Steps.class);
                startActivity(intent);
                break;
        }
    }
}
