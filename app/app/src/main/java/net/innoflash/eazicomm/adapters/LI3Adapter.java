package net.innoflash.eazicomm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.javafaker.Faker;

import net.innoflash.eazicomm.R;
import net.innoflash.eazicomm.view_holders.LI3ViewHolder;

public class LI3Adapter extends RecyclerView.Adapter<LI3ViewHolder> {

    private Context context;
    private Faker faker;

    public LI3Adapter(Context context) {
        this.context = context;
        faker = new Faker();
    }

    @NonNull
    @Override
    public LI3ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LI3ViewHolder(LayoutInflater.from(context).inflate(R.layout.single_room_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LI3ViewHolder holder, int position) {
           holder.listItem.setItemHeading(faker.number().digit() + " kids");
           holder.listItem.setItemContent(faker.color().name().toUpperCase() + " room");
    }

    @Override
    public int getItemCount() {
        return 6;
    }
}